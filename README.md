# Guide Line

Install anaconda3 to default folder (HOME/anaconda3)

copy navigator.sh & icon.png to same folder

change permission navigator.sh (chmod +x navigator.sh)

copy anaconda.desktop to default desktop folder (/usr/share/applications or ~/.local/share/applications)

edit anaconda.desktop 
"Icon=/home/ismail/anaconda3/icon.png" to "Icon=/home/your account name/anaconda3/icon.png"
"Exec="/home/ismail/anaconda3/navigator.sh" %f" to "Exec="/home/your account name/anaconda3/navigator.sh" %f" 
